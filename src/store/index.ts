import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    spending: [/* 
      {
        "id": 1,
        "category": "Health",
        "description": "Cheese",
        "transactionTime": "2018-07-04T00:12:14.716Z",
        "amount": "396.82",
        "currency": "€"
      }*/
    ]
  },
  getters: {
    ALLSPENDING: state => {
      return state.spending;
    }
  },
  mutations: {
    SET_ALLSPENDING: (state, payload) => {
      state.spending = payload;
    }
  },
  actions: {
    SAVE_ALLLSPENDING: (context, payload) => {
      context.commit ('SET_ALLSPENDING', payload);
    }
  },
  plugins: [createPersistedState()],
  modules: {
  },
});
